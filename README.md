Beaches Recovery is the premier drug and alcohol treatment center in Jacksonville Florida. The center offers a complete continuum of care. Founded on the principles of treating the mind, body and spirit, Beaches offers a wide array of treatment modalities and activities proven to combat the disease of addiction. This dual-diagnosis treatment center offers a wide range of experience in dealing with co-occurring disorders such as anxiety, depression, bi-polar, and other mood conditions.

Website : https://www.beachesrecovery.com/
